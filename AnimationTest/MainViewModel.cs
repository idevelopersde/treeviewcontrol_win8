﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AnimationTest
{
    public class Entry : INotifyPropertyChanged
    {
        private String _title;

        public String Title 
        { 
            get { return _title; }

            set
            {
                _title = value;
                if (PropertyChanged != null)
                {
                    PropertyChanged(this, new PropertyChangedEventArgs("Title"));
                }
            } 
        }

        public String Key { get; set; }
        public String ParentKey { get; set; }


        public override string ToString()
        {
            return base.ToString();
            //return Title;
        }

        public event PropertyChangedEventHandler PropertyChanged;
    }

    public class MainViewModel
    {
        private ObservableCollection<Object> _items = new ObservableCollection<Object>();
        private ObservableCollection<Object> _filteredItems = new ObservableCollection<Object>();

        public ObservableCollection<Object> Items { get { return _items; } }
        public ObservableCollection<Object> FilteredItems { get { return _filteredItems; } } 

        public MainViewModel() 
        {
            AddItems();

            _filteredItems = new ObservableCollection<object>(Items);
        }

        private void AddItems()
        {
            Items.Add(new Entry() { Title = "Item 1", Key = "1" } );
            Items.Add(new Entry() { Title = "Item 2", Key = "2" } );
            Items.Add(new Entry() { Title = "Item 3", Key = "3" } );
            Items.Add(new Entry() { Title = "Item 4", Key = "4" } );
            Items.Add(new Entry() { Title = "Item 5", Key = "5" } );

            Items.Add(new Entry() { Title = "Item 6", Key = "6", ParentKey = "1" });
            Items.Add(new Entry() { Title = "Item 7", Key = "7", ParentKey = "1" });
            Items.Add(new Entry() { Title = "Item 8", Key = "8", ParentKey = "1" });
        }
    }
}
