﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.UI;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Documents;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Animation;

// The Templated Control item template is documented at http://go.microsoft.com/fwlink/?LinkId=234235

namespace Controls
{
    internal class Group : INotifyPropertyChanged
    {
        Object _parentItem;
        ObservableCollection<Object> _items;
        Object _selectedItem;

        public Object ParentItem
        {
            get { return _parentItem; }
            set
            {
                _parentItem = value;
                if (PropertyChanged != null)
                    PropertyChanged(this, new PropertyChangedEventArgs("ParentItem"));
            }
        }

        public ObservableCollection<Object> Items
        {
            get { return _items;  }
            set
            {
                _items = value;
                if (PropertyChanged != null)
                    PropertyChanged(this, new PropertyChangedEventArgs("Items"));
            }
        }

        public Object SelectedItem
        {
            get { return _selectedItem; }
            set
            {
                _selectedItem = value;
                if (PropertyChanged != null)
                    PropertyChanged(this, new PropertyChangedEventArgs("SelectedItem"));
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
    }

    public sealed class TreeListView : Control
    {
        private FlipView _baseFlipView;
        private Storyboard _growHeaderStoryboard;
        private Storyboard _shrinkHeaderStoryboard;

		public TreeListView()
		{
			this.DefaultStyleKey = typeof(TreeListView);
            FilteredGroups = new ObservableCollection<Group>();
		}

		public static readonly DependencyProperty ItemsSourceProperty = 
            DependencyProperty.Register("ItemsSource", typeof(Object), typeof(TreeListView), new PropertyMetadata(null, ItemsSourcePropertyChanged));

        private static void ItemsSourcePropertyChanged(DependencyObject o, DependencyPropertyChangedEventArgs e)
        {
            if (o != null)
            {
                TreeListView tree = o as TreeListView;

                if (tree == null)
                    return;

                tree.Clear();
                tree.FilterItems();
            }
	    }

	    public Object ItemsSource
        {
            get { return GetValue(ItemsSourceProperty) as Object; }
            set { SetValue(ItemsSourceProperty, value); }
        }

        public static readonly DependencyProperty FilteredGroupsProperty =
            DependencyProperty.Register("FilteredGroups", typeof(ObservableCollection<Group>), typeof(TreeListView), null);

        internal ObservableCollection<Group> FilteredGroups
        {
            get { return GetValue(FilteredGroupsProperty) as ObservableCollection<Group>; }
            set { SetValue(FilteredGroupsProperty, value); }
        }

        public static readonly DependencyProperty ItemIdentifierProperty =
            DependencyProperty.Register("ItemIdentifier", typeof(String), typeof(TreeListView), null);

        public String ItemIdentifier
        {
            get { return GetValue(ItemIdentifierProperty) as String; }
            set { SetValue(ItemIdentifierProperty, value); }
        }

        public static readonly DependencyProperty ItemParentIdentifierProperty =
            DependencyProperty.Register("ItemParentIdentifier", typeof(String), typeof(TreeListView), null);

        public String ItemParentIdentifier
        {
            get { return GetValue(ItemParentIdentifierProperty) as String; }
            set { SetValue(ItemParentIdentifierProperty, value); }
        }

        public static readonly DependencyProperty SelectedItemProperty =
            DependencyProperty.Register("SelectedItem", typeof(Object), typeof(TreeListView), null);

        public Object SelectedItem
        {
            get { return GetValue(SelectedItemProperty) as Object; }
            set { SetValue(SelectedItemProperty, value); }
        }

        protected override void OnApplyTemplate()
        {
            base.OnApplyTemplate();

            _baseFlipView = GetTemplateChild("PART_TreeView_FlipView") as FlipView;
            _growHeaderStoryboard = GetTemplateChild("GrowHeaderStoryboard") as Storyboard;
            _shrinkHeaderStoryboard = GetTemplateChild("ShrinkHeaderStoryboard") as Storyboard;

            (GetTemplateChild("PART_TreeView_UpButton") as TextBlock).Tapped += (sender, args) =>
            {
                return;
            };

        }

        private void Clear()
        {
            if (_baseFlipView == null)
                return;

            _baseFlipView.Items.Clear();
        }

        private void FilterItems()
        {
            if (ItemsSource == null || !(ItemsSource is IEnumerable<object>))
                return;

            IEnumerable<object> items = ItemsSource as IEnumerable<object>;

            if (ItemIdentifier == null || ItemParentIdentifier == null)
            {
                Group group = new Group() { Items = new ObservableCollection<object>(ItemsSource as IEnumerable<object>) };
                FilteredGroups = new ObservableCollection<Group>() { group };
            }
            else
            {
                String rootKey = (SelectedItem != null) ? SelectedItem.GetType().GetRuntimeProperty(ItemIdentifier).GetValue(SelectedItem) as String : "";

                List<object> filtered;

                if (rootKey == "")
                    filtered = items.Where(x => String.IsNullOrEmpty(x.GetType().GetRuntimeProperty(ItemParentIdentifier).GetValue(x) as String)).ToList();
                else
                    filtered = items.Where(x => x.GetType().GetRuntimeProperty(ItemParentIdentifier).GetValue(x) as String == rootKey).ToList();

                Group newGroup = new Group()
                {
                    ParentItem = SelectedItem,
                    Items = new ObservableCollection<Object>(filtered)
                };

                newGroup.PropertyChanged += newGroup_PropertyChanged;

                FilteredGroups.Add(newGroup);
            }
        }

        private void UpButton_Tapped(object sender, PropertyChangedEventArgs e)
        {
            return;
        }

        void newGroup_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName != "SelectedItem")
                return;

            _growHeaderStoryboard.Completed += (o, o1) =>
            {
                Group group = sender as Group;
                SelectedItem = group.SelectedItem;
                FilterItems();
                JumpToLastGroup();
            };

            _growHeaderStoryboard.Begin();
        }

        private void JumpToLastGroup()
        {
            _baseFlipView.SelectedItem = _baseFlipView.Items.Last();
        }
    }
}
