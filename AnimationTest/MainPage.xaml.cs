﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace AnimationTest
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        public MainPage()
        {
            this.InitializeComponent();
        }

        private void ShowButton_OnTapped(object sender, TappedRoutedEventArgs e)
        {
            //Header.Visibility = Visibility.Visible;
            //EnterStoryboard.Begin();
            //Header.Visibility = Visibility.Visible;
            //PopInStoryboard.Begin();
			GrowStoryboard.Begin();
        }

        private void HideButton_OnTapped(object sender, TappedRoutedEventArgs e)
        {
            //Header.Visibility = Visibility.Collapsed;
            //ExitStoryboard.Begin();
            PopOutStoryboard.Completed += (o, o1) =>
            {
                Header.Visibility = Visibility.Collapsed;
            };
            PopOutStoryboard.Begin();
        }
    }
}
